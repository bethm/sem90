\documentclass{beamer}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[ngerman]{babel} 
\usepackage{fancyvrb}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{libertine}
\usepackage{courier}
\usepackage{hyperref}
\usepackage[style=authoryear,backend=biber]{biblatex}
\usepackage[babel]{csquotes}
\usepackage{booktabs}
\usepackage{multirow}
\addbibresource{literatur.bib}
\renewcommand*{\bibfont}{\footnotesize}

\makeatletter
\def\url@foostyle{%
  \@ifundefined{selectfont}{\def\UrlFont{\sf}}{\def\UrlFont{\footnotesize\ttfamily}}}
\makeatother
\urlstyle{sf}

\usepackage{tikz}
\usetikzlibrary{shapes,arrows,decorations.pathmorphing,backgrounds,positioning,fit,petri}
\tikzstyle{latent}=[circle,draw=black,semithick,inner sep=0pt,minimum size=10mm]
\tikzstyle{manifest}=[rectangle,draw=black,semithick,inner sep=0pt,minimum size=10mm]
\tikzstyle{const}=[regular polygon,regular polygon sides=3,draw=black!30,semithick,inner sep=0pt,minimum size=10mm]
\tikzstyle{resid}=[inner sep=0pt,minimum size=4mm]
\tikzstyle{corr}=[<->,shorten >=1pt,shorten <=1pt,>=stealth',semithick]
\tikzstyle{coef}=[->,shorten >=1pt,shorten <=1pt,>=stealth',semithick]
%\tikzset{venn circle/.style={draw,circle,minimum width=2cm,fill=#1,opacity=0.6}}
\tikzset{venn circle/.style={draw,circle,minimum width=2cm}}

%-------------------------------------------------------------------
\beamertemplatenavigationsymbolsempty
\makeatletter
\setbeamertemplate{footline}{
  \leavevmode%
  \hbox{%
  \begin{beamercolorbox}[wd=.333333\paperwidth,ht=.5cm,dp=1ex,left]
    {author in head/foot}%
    \hspace{.05cm}
    \includegraphics[height=0.15cm]{80x15.png}
    \hspace{.05cm}
    \usebeamerfont{author in head/foot}\insertauthor
  \end{beamercolorbox}%
  \begin{beamercolorbox}[wd=.666666\paperwidth,ht=.5cm,dp=1ex,right]
    {date in head/foot}%
    \vspace{-.1cm}
    \includegraphics[height=0.3cm]{150px-Uni_Mannheim_Siegel.png}
    \hspace{.05cm}
    \includegraphics[height=0.3cm]{sswml_logo2.png}
  \end{beamercolorbox}}%
  \vskip0pt%
}
\makeatother

%-------------------------------------------------------------------
\RecustomVerbatimEnvironment
  {Verbatim}{Verbatim}
  {frame=single,gobble=4,formatcom=\color{blue!50!black},}
%fontsize=\footnotesize
%-------------------------------------------------------------------
\title{Latent Variable Modelling in a Nutshell} 
\subtitle{Oder: Strukturgleichungsmodelle in 90 Minuten}
\author{Arne Bethmann}
\date{\footnotesize \url{https://gitlab.com/bethm/sem90}}

%-------------------------------------------------------------------
\begin{document}

%-------------------------------------------------------------------
\begin{frame}
  \titlepage
\end{frame}

%-------------------------------------------------------------------
\begin{frame}
  \frametitle{Überblick}
  \tableofcontents
\end{frame}

\section{Statistische Modellierung}
%-------------------------------------------------------------------
\begin{frame}
  \frametitle{Statistische Modellierung}
  
  \begin{itemize}
  \item Was ist ein Modell?
  \end{itemize}
\pause
  \vspace{2em}
  \centering
  \begin{tikzpicture}   
    \node [venn circle = green] (t) {Theorie};
    \node [venn circle = red]   (m) [right=1.5 of t] {Modell};
    \node [venn circle = blue]  (d) [right=1.5 of m] {Daten};
    \draw [corr, thick] (t) -- (m);
    \draw [corr, thick] (m) -- (d);
  \end{tikzpicture}  
\end{frame}


%-------------------------------------------------------------------
\begin{frame}
  \frametitle{Latente Variablen}
  \begin{itemize}
    \item \textbf{Meßfehler} z.~B. Faktormodelle, Item-Response-Modelle, medizinische Diagnostik
    \item \textbf{Hypothetische Konstrukte} z.~B. Latente Eigenschaften, latente Klassen, Clusteranalyse
    \item \textbf{Unbeobachtete Heterogenität} z.~B. Panelmodelle, Mehr-Ebenen-Modelle, Frailty-Modelle
    \item \textbf{Fehlende Werte} z.~B. Sample-Selection-Modelle (Heckman etc.),
    \item \textbf{Latente Antworten} z.~B. Generalized Linear Models (Logit, Probit, Ordinal etc.)
  \end{itemize}
\emph{\footnotesize vgl. \textcite{skrondal200401}}
\end{frame}


%-------------------------------------------------------------------
\begin{frame}
  \frametitle{Symbole zur Modellbeschreibung}
  \centering
  \vspace{1em}
  \begin{tikzpicture}   
    \node[manifest] (man) {};
    \node [right=.45 of man] {= manifeste bzw. beobachtete Variablen};
    \node[latent] (lat) [below=.4 of man] {};
    \node [right=.45 of lat] {= latente bzw. indirekt gemessene Variablen};
    \node[resid] (res1) [below left=.7 and .1 of lat] {};
    \node[resid] (res2) [right=1 of res1] {};
    \draw [coef] (res1) -- (res2);
    \node [right=0 of res2] {= gerichtete Zusammenhänge};
    \node[resid] (cor1) [below left=1.9 and .1 of lat] {};
    \node[resid] (cor2) [right=1 of cor1] {};
    \draw [corr] (cor1) -- (cor2);
    \node [right=0 of cor2] {= ungerichtete Zusammenhänge};
%    \node[const] (con) [below=2.5 of lat] {\color{black!30} 1};
%    \node [right=.65 of con] {\color{black!30}= Konstanten bzw. Intercepts};
  \end{tikzpicture}
  \vspace{1em}
  \begin{itemize}
    \item für zu schätzende Größen werden griechische Buchstaben 
      verwendet, für gegebene Größen lateinische
  \end{itemize}
\end{frame}


\section{Regression}
%-------------------------------------------------------------------
\begin{frame}
  \frametitle{Einfaches Regressionsmodell}
  \centering
  \vspace{2em}
  \begin{tikzpicture}   

    % abhängige Variable
    \node[manifest, label=$\alpha$] (dep1) {$y$};

    % unabhängige Variablen
    \node[manifest] (ind1) [left=2 of dep1] {$x$};

    % Residuum
    \node[resid] (resdep1) [right=.5 of dep1] {$\epsilon$};

    % Zusammenhänge
    \draw [coef] (ind1) to node[auto] {$\beta$} (dep1); 
    \draw [coef] (resdep1) -- (dep1); 

  \end{tikzpicture} \\
  \vspace{1em}

  \begin{description}
    \item[$x$] unabhängige bzw. exogene Variable, da \emph{kein} 
      gerichteter Pfeil auf sie zeigt
    \item[$y$]abhängige bzw. endogene Variable, da 
      \emph{mindestens ein} gerichteter Pfeil auf sie zeigt
    \item [$\alpha$] Konstante bzw. Intercept der im Modell zu 
      schätzen ist
    \item [$\beta$] Regressionskoeffizient der im Modell zu 
      schätzen ist
    \item [$\epsilon$] Residuum bzw. Fehlerterm, jede manifeste, abhängige 
      Variable hat eins
 
  \end{description}
  \vspace{1em}
  Modellgleichung: $y_i = \alpha + \beta x_{i} + \epsilon_i$
\end{frame}

%-------------------------------------------------------------------
\begin{frame}[fragile]
  \frametitle{Einfaches Regressionsmodell}
  Stata-Syntax:
  \begin{Verbatim}
    regress y x
    sem x -> y
  \end{Verbatim} 
\pause
  Stata-Beispiel (1/2):
  \fvset{frame=single,fontsize=\tiny}
  \VerbatimInput[firstline=33,lastline=51]{../log/b_ex.log}  
\end{frame}

\begin{frame}[fragile]
  \frametitle{Einfaches Regressionsmodell}
  Stata-Beispiel (2/2):
  \fvset{frame=single,fontsize=\tiny}
  \VerbatimInput[firstline=53,lastline=64]{../log/b_ex.log}  
  \vspace{.5em}
  Geschätzte Parameter: \\
  \vspace{.5em}
  \begin{tabular}{rr}
            & \_cons \\
   $\alpha$ & 1,518  \\
  \end{tabular}
  \hspace{2em}
  \begin{tabular}{rr}
           & geschl \\
   $\beta$ & -0,119 \\
  \end{tabular}
  \hspace{2em}
  \begin{tabular}{rr}
                   & item1 \\
   $var(\epsilon)$ & 0,425 \\
  \end{tabular}
  
  \vspace{1em}
  \centering
  \begin{tikzpicture}   

    % abhängige Variable
    \node[manifest, label={\footnotesize 1,518}] (dep1) {\footnotesize item1};

    % unabhängige Variablen
    \node[manifest] (ind1) [left=2 of dep1] {\footnotesize geschl};

    % Residuum
    \node[resid] (resdep1) [right=.5 of dep1] {\footnotesize 0,425};

    % Zusammenhänge
    \draw [coef] (ind1) to node[auto] {\footnotesize -0,119} (dep1); 
    \draw [coef] (resdep1) -- (dep1); 

  \end{tikzpicture} \\ 
\end{frame}



%-------------------------------------------------------------------
\begin{frame}
  \frametitle{Multiples Regressionsmodell}
  \centering
  \vspace{2em}
  \begin{tikzpicture}   

    % abhängige Variable

    \node[manifest, label=$\alpha$] (dep1) {$y$};

    % unabhängige Variablen
    \node[manifest] (ind2) [left=2 of dep1] {$x_2$};
    \node[manifest] (ind1) [above=of ind2] {$x_1$};
    \node[manifest] (ind3) [below=of ind2] {$x_3$};

    % Residuum
    \node[resid] (resdep1) [right=.5 of dep1] {$\epsilon$};

    % Zusammenhänge
    \draw [coef] (ind1) to node[auto] {$\beta_1$} (dep1); 
    \draw [coef] (ind2) to node[auto] {$\beta_2$} (dep1); 
    \draw [coef] (ind3) to node[auto] {$\beta_3$} (dep1); 
    \draw [coef] (resdep1) -- (dep1); 

  \end{tikzpicture} \\
  \vspace{1em}
  Modellgleichung: $y_i = \alpha + \beta_1 x_{1i} + \beta_2 x_{2i} + \beta_3 x_{3i} + \epsilon_i$ 
\end{frame}

%-------------------------------------------------------------------
\begin{frame}[fragile]
  \frametitle{Multiples Regressionsmodell}
  Stata-Syntax:
  \begin{Verbatim}
    regress y x1 x2 x3
    sem x1 x2 x3 -> y
  \end{Verbatim} 
\pause
  Stata-Beispiel (1/2):
  \fvset{frame=single,fontsize=\tiny}
  \VerbatimInput[firstline=86,lastline=104]{../log/b_ex.log}  
\end{frame}

\begin{frame}[fragile]
  \frametitle{Multiples Regressionsmodell}
  Stata-Beispiel (2/2):
  \fvset{frame=single,fontsize=\tiny}
  \VerbatimInput[firstline=106,lastline=119]{../log/b_ex.log}
  \vspace{1em}
  Geschätzte Parameter: \\
  \vspace{.5em}
  \begin{tabular}{rr}
            & \_cons \\
   $\alpha$ & 1,797  \\
  \end{tabular}
  \hspace{.2em}
  \begin{tabular}{rrrr}
                      & geschl & alter  & ostwest \\
  $\beta_{1 \dots 3}$ & -0,116 & 0,0005 & -0,233  \\
  \end{tabular}
  \hspace{.2em}
  \begin{tabular}{rr}
                  & item1 \\
  $var(\epsilon)$ & 0,414 \\ 
  \end{tabular}
\end{frame}


\section{Faktoranalyse}
%-------------------------------------------------------------------
\begin{frame}
\frametitle{Einfaches Messmodell}
  \centering
  \vspace{2em}
  \begin{tikzpicture}   

  % Indikatoren  
  \node[manifest, label=$\alpha$] (dep1) {$y$};

  % Faktor
  \node[latent] (ind1) [left=2 of dep1] {$\eta$};

  % Residuen
  \node[resid] (resdep1) [right=.5 of dep1] {$\epsilon$};

  % Zusammenhänge
  \draw [coef] (ind1) to node[auto] {$\lambda$} (dep1);
  \draw [coef] (resdep1) -- (dep1);

  \end{tikzpicture} \\
  \vspace{1em}
  
  \begin{description}
  \item[$\eta$] exogener bzw. unabhängiger, latenter Faktor bzw. wahrer Wert
  \item[$y$]abhängiger bzw. endogener, manifester Indikator
  \item [$\lambda$] Faktorladung
  \item [$\epsilon$] Meßfehler bzw. Residuum
  \end{description}
  \vspace{1em}
  Modellgleichung: $y_i = \alpha + \lambda \eta + \epsilon_i$
\end{frame}

%-------------------------------------------------------------------
\begin{frame}
\frametitle{Konfirmatorische Faktoranalyse (CFA)}
  \centering
  \vspace{2em}
  \begin{tikzpicture}   

  % Indikatoren  
  \node[manifest, label=$\alpha_1$] (dep1) {$y_{1}$};
  \node[manifest, label=$\alpha_2$] (dep2) [below=of dep1] {$y_{2}$};
  \node[manifest, label=$\alpha_3$] (dep3) [below=of dep2] {$y_{3}$};

  % Faktor
  \node[latent] (ind1) [left=2 of dep2] {$\eta$};

  % Residuen
  \node[resid] (resdep1) [right=.5 of dep1] {$\epsilon_{1}$};
  \node[resid] (resdep2) [right=.5 of dep2] {$\epsilon_{2}$};
  \node[resid] (resdep3) [right=.5 of dep3] {$\epsilon_{3}$};

  % Zusammenhänge
  \draw [coef] (ind1) to node[auto] {$\lambda_{1}$} (dep1);
  \draw [coef] (ind1) to node[auto] {$\lambda_{2}$} (dep2);
  \draw [coef] (ind1) to node[auto] {$\lambda_{3}$} (dep3);
  \draw [coef] (resdep1) -- (dep1);
  \draw [coef] (resdep2) -- (dep2);
  \draw [coef] (resdep3) -- (dep3);

  \end{tikzpicture} \\
  \begin{columns}[c]
  \column{.1\linewidth}
  \column{.4\linewidth}
    \flushright
    Modellgleichungen:
  \column{.4\linewidth}
    \flushleft
    $y_{1i} = \alpha_1 + \lambda_{1} \eta + \epsilon_{1i}$ \\
    $y_{2i} = \alpha_2 + \lambda_{2} \eta + \epsilon_{2i}$ \\
    $y_{3i} = \alpha_3 + \lambda_{3} \eta + \epsilon_{3i}$ \\
  \column{.1\linewidth}
  \end{columns}
\end{frame}


%-------------------------------------------------------------------
\begin{frame}[fragile]
  \frametitle{Konfirmatorische Faktoranalyse (CFA)}
  Stata-Syntax:
  \begin{Verbatim}
    sem y1 y2 y3 <- ETA
  \end{Verbatim} 
\pause
  Stata-Beispiel (1/2):
  \fvset{frame=single,fontsize=\tiny}
  \VerbatimInput[firstline=127,lastline=145]{../log/b_ex.log}  
\end{frame}

\begin{frame}[fragile]
  \frametitle{Konfirmatorische Faktoranalyse (CFA)}
  Stata-Beispiel (2/2):
  \fvset{frame=single,fontsize=\tiny}
  \VerbatimInput[firstline=147,lastline=170]{../log/b_ex.log}  
\end{frame}

\begin{frame}[fragile]
  \frametitle{Konfirmatorische Faktoranalyse (CFA)}
  Geschätzte Parameter: \\
  \vspace{.5em}
  \begin{tabular}{rrrr}
  \midrule
                        & item1 & item2 & item3 \\
   $\alpha_{1 \dots 3}$ & 1,337 & 3,151 & 2,695  \\
  \addlinespace
                        & item1    & item2  & item3   \\
  $\lambda_{1 \dots 3}$ & \emph{1} & -1,129 & -3,024  \\
  \addlinespace
                  & item1 & item2 & item3 \\
  $var(\epsilon_{1 \dots 3})$ & 0,345 & 0,596 & 0,307 \\ 
  \addlinespace
              & GR \\
  $var(\eta)$ & 0,345 \\ 
  \midrule
  \end{tabular}
\end{frame}


%-------------------------------------------------------------------
\begin{frame}
\frametitle{Latente Regression bzw. CFA mit Kovariaten}
  \centering

  \begin{tikzpicture}   

  % Faktor
%  \node[latent, label=$\alpha_4$] (lat1) {$\eta$};
  \node[latent] (lat1) {$\eta$};

  % Indikatoren  
  \node[manifest, label=$\alpha_2$] (dep2) [right=2 of lat1] {$y_{2}$};
  \node[manifest, label=$\alpha_1$] (dep1) [above=of dep2]   {$y_{1}$};
  \node[manifest, label=$\alpha_3$] (dep3) [below=of dep2]   {$y_{3}$};

  % unabhängige Variablen
  \node[manifest] (ind2) [below left=0 and 2 of lat1] {$x_2$};
  \node[manifest] (ind1) [above left=0 and 2 of lat1] {$x_1$};

  % Residuen
  \node[resid] (resdep1) [right=.5 of dep1] {$\epsilon_{1}$};
  \node[resid] (resdep2) [right=.5 of dep2] {$\epsilon_{2}$};
  \node[resid] (resdep3) [right=.5 of dep3] {$\epsilon_{3}$};
%  \node[resid] (reslat1) [below=.5 of lat1] {$\epsilon_{4}$};

  % Zusammenhänge
  \draw [coef] (lat1) to node[auto] {$\lambda_{1}$} (dep1);
  \draw [coef] (lat1) to node[auto] {$\lambda_{2}$} (dep2);
  \draw [coef] (lat1) to node[auto] {$\lambda_{3}$} (dep3);
  \draw [coef] (ind1) to node[auto] {$\beta_{1}$} (lat1);
  \draw [coef] (ind2) to node[auto] {$\beta_{2}$} (lat1);
  \draw [coef] (resdep1) -- (dep1);
  \draw [coef] (resdep2) -- (dep2);
  \draw [coef] (resdep3) -- (dep3);
%  \draw [coef] (reslat1) -- (lat1);

  \end{tikzpicture} \\
  \vspace{1em}
  \begin{columns}[c]
  \column{.1\linewidth}
  \column{.4\linewidth}
    \centering
    Strukturmodell: \\
%    $\eta = \alpha_4 + \beta_1 x_{1i} + \beta_2 x_{2i} + \epsilon_4$
    $\eta = \beta_1 x_{1i} + \beta_2 x_{2i}$
  \column{.4\linewidth}
    \centering
    Messmodell: \\
    $y_{1i} = \alpha_1 + \lambda_{1} \eta + \epsilon_{1i}$ \\
    $y_{2i} = \alpha_2 + \lambda_{2} \eta + \epsilon_{2i}$ \\
    $y_{3i} = \alpha_3 + \lambda_{3} \eta + \epsilon_{3i}$ \\
  \column{.1\linewidth}
  \end{columns} 
\end{frame}

%-------------------------------------------------------------------
\begin{frame}[fragile]
  \frametitle{Latente Regression bzw. CFA mit Kovariaten}
  Stata-Syntax:
  \begin{Verbatim}
    sem (y1 y2 y3 <- ETA) (x1 x2 -> ETA)
  \end{Verbatim} 
\pause
  Stata-Beispiel (1/2):
  \fvset{frame=single,fontsize=\tiny}
  \VerbatimInput[firstline=174,lastline=189]{../log/b_ex.log}
  \VerbatimInput[firstline=201,lastline=205]{../log/b_ex.log}  
\end{frame}

\begin{frame}[fragile]
  \frametitle{Latente Regression bzw. CFA mit Kovariaten}
  Stata-Beispiel (2/2):
  \fvset{frame=single,fontsize=\tiny}
  \VerbatimInput[firstline=207,lastline=235]{../log/b_ex.log}  
\end{frame}

\begin{frame}[fragile]
  \frametitle{Latente Regression bzw. CFA mit Kovariaten}
  Geschätzte Parameter: \\
  \vspace{.5em}
  \begin{tabular}{rrrr}
  \midrule
                        & item1 & item2 & item3 \\
   $\alpha_{1 \dots 3}$ & 1,691 & 2,765 & 1,513 \\
  \addlinespace
                        & geschl & ostwest & \\
   $\beta_{1 \dots 2}$  & -0,088 & -0,168  & \\
  \addlinespace
                        & item1    & item2  & item3   \\
  $\lambda_{1 \dots 3}$ & \emph{1} & -1,091 & -3,341  \\
  \addlinespace
                              & item1 & item2 & item3 \\
  $var(\epsilon_{1 \dots 3})$ & 0,352 & 0,611 & 0,213 \\ 
  \addlinespace
              & GR \\
  $var(\eta)$ & 0,068 \\ 
  \midrule
  \end{tabular}
\end{frame}


\section{Pfadanalyse}
%-------------------------------------------------------------------
\begin{frame}
  \frametitle{Einfaches Pfadmodell}
  \centering
  \vspace{.5em}
  \begin{tikzpicture}   

    % unabhängige Variablen
    \node[manifest] (ind1) {$x$};

    % abhängige Variable
    \node[manifest, label=$\alpha_1$] (dep1) [above right=1 and 1 of ind1] {$y_1$};
    \node[manifest, label=$\alpha_2$] (dep2) [below right=1 and 1 of dep1] {$y_2$};

    % Residuum
    \node[resid] (resdep1) [right=.5 of dep1] {$\epsilon_1$};
    \node[resid] (resdep2) [right=.5 of dep2] {$\epsilon_2$};

    % Zusammenhänge
    \draw [coef] (ind1) to node[auto] {$\beta_1$} (dep1); 
    \draw [coef] (dep1) to node[auto] {$\beta_2$} (dep2); 
    \draw [coef] (ind1) to node[auto] {$\beta_3$} (dep2); 
    \draw [coef] (resdep1) -- (dep1); 
    \draw [coef] (resdep2) -- (dep2); 

  \end{tikzpicture} \\
  \vspace{.5em}
  \begin{description}
    \item[$x$] unabhängige bzw. exogene Variable
    \item[$y_1$, $y_2$] abhängige bzw. endogene Variablen
    \item [$\alpha_1$, $\alpha_2$] Konstante bzw. Intercept
    \item [$\beta_1$, $\beta_2$, $\beta_3$] Regressionskoeffizienten
    \item [$\epsilon_1$, $\epsilon_2$] Residuen bzw. Fehlerterme
  \end{description}
%  \vspace{.5em}
  \begin{columns}[c]
  \column{.1\linewidth}
  \column{.4\linewidth}
    \flushright
    Modellgleichungen:
  \column{.4\linewidth}
    \flushleft
    $y_{1i} = \alpha_1 + \beta_1 x_i + \epsilon_{1i}$ \\
    $y_{2i} = \alpha_2 + \beta_2 y_{1i} + \beta_3 x_i + \epsilon_{2i}$ \\
  \column{.1\linewidth}
  \end{columns}
\end{frame}

%-------------------------------------------------------------------
\begin{frame}[fragile]
  \frametitle{Einfaches Pfadmodell}
  Stata-Syntax:
  \begin{Verbatim}
    sem (x y1 -> y2) (x -> y1)
  \end{Verbatim} 
\pause
  Stata-Beispiel (1/2):
  \fvset{frame=single,fontsize=\tiny}
  \VerbatimInput[firstline=239,lastline=257]{../log/b_ex.log}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Einfaches Pfadmodell}
  Stata-Beispiel (2/2):
  \fvset{frame=single,fontsize=\tiny}
  \VerbatimInput[firstline=259,lastline=276]{../log/b_ex.log}  
\end{frame}

\begin{frame}[fragile]
  \frametitle{Einfaches Pfadmodell}
  Geschätzte Parameter: \\
  \vspace{.5em}
  {\footnotesize
  \begin{tabular}{rccc}
  \midrule
                        & ostwest & item1 \\
   $\alpha_{1 \dots 2}$ & 1,307   & 1,820 \\
  \addlinespace
                        & geschl > ostwest & ostwest > item1 & geschl > item1 \\
   $\beta_{1 \dots 3}$  & 0,008    & -0,231    & -0,117 \\
  \addlinespace
                              & ostwest & item1 \\
  $var(\epsilon_{1 \dots 2})$ & 0,217   & 0,414 \\ 
  \midrule
  \end{tabular}
  }

  \begin{center}
%  \vspace{.5em}
  \scalebox{.9}{
  \begin{tikzpicture}   

    % unabhängige Variablen
    \node[manifest] (ind1) {\footnotesize geschl};

    % abhängige Variable
    \node[manifest, label={\footnotesize 1,307}] (dep1) [above right=1 and 1 of ind1] {\footnotesize ostwest};
    \node[manifest, label={\footnotesize 1,820}] (dep2) [below right=1 and 1 of dep1] {\footnotesize item1};

    % Residuum
    \node[resid] (resdep1) [right=.5 of dep1] {\footnotesize 0,217};
    \node[resid] (resdep2) [right=.5 of dep2] {\footnotesize 0,414};

    % Zusammenhänge
    \draw [coef] (ind1) to node[auto] {\footnotesize 0,008} (dep1); 
    \draw [coef] (dep1) to node[auto] {\footnotesize -0,231} (dep2); 
    \draw [coef] (ind1) to node[auto] {\footnotesize -0,117} (dep2); 
    \draw [coef] (resdep1) -- (dep1); 
    \draw [coef] (resdep2) -- (dep2); 

  \end{tikzpicture}
  }
  \end{center}
\end{frame}


\section{Strukturgleichungsmodelle}
%-------------------------------------------------------------------
\begin{frame}
  \frametitle{(Einfaches) Strukturgleichungsmodell}
  \centering
 \vspace{.5em}
 \begin{tikzpicture}   

  % unabhängige Variablen
  \node[manifest] (ind1) {$x$};

  \node[manifest, label=$\alpha_1$] (dep1) [above right=1 and 1 of ind1] {$y_1$};

  % Faktor
%  \node[latent, label=$\alpha_5$] (lat1) [below right=1.15 and 1 of dep1] {$\eta$};
  \node[latent] (lat1) [below right=1.15 and 1 of dep1] {$\eta$};

  % Indikatoren  
  \node[manifest, label=$\alpha_3$] (dep3) [right=2 of lat1] {$y_{3}$};
  \node[manifest, label=$\alpha_2$] (dep2) [above=of dep3]   {$y_{2}$};
  \node[manifest, label=$\alpha_4$] (dep4) [below=of dep3]   {$y_{4}$};

  % Residuen
  \node[resid] (resdep1) [right=.5 of dep1] {$\epsilon_{1}$};
  \node[resid] (resdep2) [right=.5 of dep2] {$\epsilon_{2}$};
  \node[resid] (resdep3) [right=.5 of dep3] {$\epsilon_{3}$};
  \node[resid] (resdep4) [right=.5 of dep4] {$\epsilon_{4}$};
%  \node[resid] (reslat1) [below=.5 of lat1] {$\epsilon_{5}$};

  % Zusammenhänge
  \draw [coef] (lat1) to node[auto] {$\lambda_{1}$} (dep2);
  \draw [coef] (lat1) to node[auto] {$\lambda_{2}$} (dep3);
  \draw [coef] (lat1) to node[auto] {$\lambda_{3}$} (dep4);
  \draw [coef] (ind1) to node[auto] {$\beta_{1}$} (dep1);
  \draw [coef] (dep1) to node[auto] {$\beta_{2}$} (lat1);
  \draw [coef] (ind1) to node[auto] {$\beta_{3}$} (lat1);
  \draw [coef] (resdep1) -- (dep1);
  \draw [coef] (resdep2) -- (dep2);
  \draw [coef] (resdep3) -- (dep3);
  \draw [coef] (resdep4) -- (dep4);
%  \draw [coef] (reslat1) -- (lat1);

  \end{tikzpicture} \\
  \vspace{1em}
  \begin{columns}[c]
  \column{.1\linewidth}
  \column{.4\linewidth}
    \centering
    Strukturmodell: \\
    $y_1 = \alpha_1 + \beta_1 x_i + \epsilon_i$ \\
%    $\eta = \alpha_5 + \beta_2 y_{1i} + \beta_3 x_{i} + \epsilon_5$
    $\eta = \beta_2 y_{1i} + \beta_3 x_{i}$
  \column{.4\linewidth}
    \centering
    Messmodell: \\
    $y_{2i} = \alpha_2 + \lambda_{2} \eta + \epsilon_{2i}$ \\
    $y_{3i} = \alpha_3 + \lambda_{3} \eta + \epsilon_{3i}$ \\
    $y_{4i} = \alpha_4 + \lambda_{4} \eta + \epsilon_{4i}$ \\
  \column{.1\linewidth}
  \end{columns} 
\end{frame}


%-------------------------------------------------------------------
\begin{frame}[fragile]
  \frametitle{(Einfaches) Strukturgleichungsmodell}
  Stata-Syntax:
  \begin{Verbatim}
    sem (y2 y3 y4 <- ETA) (x y1 -> ETA) (x -> y1)
  \end{Verbatim} 
\pause
  Stata-Beispiel (1/3):
  \fvset{frame=single,fontsize=\tiny}
  \VerbatimInput[firstline=280,lastline=297]{../log/b_ex.log}
  \VerbatimInput[firstline=309,lastline=313]{../log/b_ex.log}  
\end{frame}

\begin{frame}[fragile]
  \frametitle{(Einfaches) Strukturgleichungsmodell}
  Stata-Beispiel (2/3):
  \fvset{frame=single,fontsize=\tiny}
  \VerbatimInput[firstline=315,lastline=340]{../log/b_ex.log}  
\end{frame}

\begin{frame}[fragile]
  \frametitle{(Einfaches) Strukturgleichungsmodell}
  Stata-Beispiel (3/3):
  \fvset{frame=single,fontsize=\tiny}
  \VerbatimInput[firstline=341,lastline=348]{../log/b_ex.log}  
\end{frame}

\begin{frame}[fragile]
  \frametitle{(Einfaches) Strukturgleichungsmodell}
  Geschätzte Parameter: \\
  \vspace{.5em}
  {\footnotesize
  \begin{tabular}{rrrrr}
  \midrule
                        & ostwest & item1 & item2 & item3 \\
   $\alpha_{1 \dots 4}$ & 1,310   & 1,691 & 2,765 & 1,513 \\
  \addlinespace
                        & geschl > & ostwest > & geschl > \\
                        & ostwest  & GR        & GR       \\
   $\beta_{1 \dots 2}$  & 0,008    & -0,168    & -0,875   \\
  \addlinespace
                        & item1    & item2  & item3   \\
  $\lambda_{1 \dots 3}$ & \emph{1} & -1,091 & -3,341  \\
  \addlinespace
                              & ostwest & item1 & item2 & item3 \\
  $var(\epsilon_{1 \dots 4})$ & 0,218   & 0,352 & 0,611 & 0,213 \\ 
  \addlinespace
              & GR \\
  $var(\eta)$ & 0,068 \\ 
  \midrule
  \end{tabular}
  }
\end{frame}


%-------------------------------------------------------------------
\begin{frame}
  \frametitle{Weitere wichtige(!) Themen}
  \begin{itemize}
    \item Freiheitsgrade
    \begin{itemize}
      \item Um Modell schätzen zu können muss es identifiziert sein, d.~h.: \\ mehr gemessene Größen (q) als zu schätzende Größen (p)
      \item q = Elemente der Varianz/Kovarianzmatrix + Mittelwerte \\ bzw. $\frac{\text{Anz. manif. Var.} \times (\text{Anz. manif. Var.} + 1)}{2} + \text{Anz. manif. Var.}$
      \item p = (meistens) Anzahl der griechischen Buchstaben
      \item Freiheitsgrade (degrees of freedom; df) = $q - p$
    \end{itemize}
    \item Restriktionen
    \item Güte der Modellanpassung (\Verb+estat gof+)
    \item Meßinvarianz (\Verb+sem ..., group(...) ginvariant(...)+)
    \item Nicht-metrische Indikatoren (\Verb+gsem+)
    \item Mehr-Ebenen-Modelle (\Verb+gsem ... M1[...]+)
    \item Nicht-metrische latente Variablen (Mplus, LatenGOLD, LISREL etc.)
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{One more thing \dots}
  Stata SEM Builder \\
  \vspace{.5em}
  \includegraphics[scale=.42]{sem-ex.png} \\
  \vspace{-.2em}
  {\tiny \emph{Quelle: \url{http://www.stata.com/features/structural-equation-modeling/}}}
\end{frame}


%-------------------------------------------------------------------
\begin{frame}[fragile]
  \frametitle{Literatur}
  \nocite{joereskog197301,muthen200201,skrondal2007latent,stata2013_sem,skrondal200401,reinecke201401,rabehesketh201201}
  \footnotesize
  \printbibliography
\end{frame}



%-------------------------------------------------------------------
\begin{frame}
  \frametitle{Pfadmodell mit Kovariaten}
  \centering
  \vspace{2em}
  \begin{tikzpicture}   

    % unabhängige Variablen
    \node[manifest] (ind2) {$x_2$};
    \node[manifest] (ind1) [above=of ind2] {$x_1$};
    \node[manifest] (ind3) [below=of ind2] {$x_3$};

    % abhängige Variable
    \node[manifest] (dep1) [above right=.3 and 3 of ind2] {$y_1$};
    \node[manifest] (dep2) [below right=.3 and 3 of ind2] {$y_2$};


    % Residuum
    \node[resid] (resdep1) [right=.5 of dep1] {$\epsilon_1$};
    \node[resid] (resdep2) [right=.5 of dep2] {$\epsilon_2$};

    % Zusammenhänge
    \draw [coef] (ind1) to node[auto] {$\beta_1$} (dep1); 
    \draw [coef] (ind2) to node[auto] {$\beta_2$} (dep1); 
    \draw [coef] (ind3) to node[auto] {$\beta_3$} (dep1); 
    \draw [coef] (ind2) to node[auto] {$\beta_4$} (dep2); 
    \draw [coef] (ind3) to node[auto] {$\beta_5$} (dep2); 
    \draw [coef] (dep1) to node[auto] {$\beta_6$} (dep2); 
    \draw [coef] (resdep1) -- (dep1); 
    \draw [coef] (resdep2) -- (dep2); 

  \end{tikzpicture} \\
  \vspace{1em}

  Formeln: \\
  $y_{1i} = \beta_1 x_{1i} + \beta_2 x_{2i} + 
    \beta_3 x_{3i} + \epsilon_{1i}$ \\
  $y_{2i} = \beta_4 x_{2i} + \beta_5 x_{3i} + 
    \beta_6 y_{1i} + \epsilon_{2i}$ \\
\end{frame}


\begin{frame}
  \frametitle{Einfaches Mehrebenenmodell}
  \centering
  \vspace{2em}
  \begin{tikzpicture}   
    % Ebenen
    \draw (-3.75,.75)   rectangle (1.5,-.75);
    \draw (1.25,-0.5) node {$i$};
    \draw (-4,1)   rectangle (1.75,-2.25);
    \draw (1.5,-2) node {$j$};
    
    % abhängige Variable
    \node[manifest] (dep1) {$y$};

    % unabhängige Variablen
    \node[manifest] (ind1) [left=2 of dep1] {$x$};
    \node[latent]   (ind2) [below=.5 of dep1] {$\zeta$};
    
    % Residuum
    \node[resid] (resdep1) [right=.5 of dep1] {$\epsilon$};
    \node[resid] (resdep1) [right=.5 of dep1] {$\epsilon$};
    \node[resid] (resdep1) [right=.5 of dep1] {$\epsilon$};

    % Zusammenhänge
    \draw [coef] (ind1) to node[auto] {$\beta$} (dep1); 
    \draw [coef] (ind2) to (dep1); 
    \draw [coef] (resdep1) -- (dep1); 

  \end{tikzpicture} \\
  \vspace{1em}

  \begin{description}
    \item[$\zeta$] Fehlerterm auf der zweiten Ebene
  \end{description}
  \vspace{1em}
  Formel: $y_{ij} = \beta x_{ij} + \zeta_j + \epsilon_{ij}$
\end{frame}

%-------------------------------------------------------------------
\begin{frame}
  \frametitle{Komplexes Mehrebenemodell}
  \centering
  \vspace{2em}
  \begin{tikzpicture}   
    % Ebenen
    \draw (-.75,.75)   rectangle (4.5,-2.75);
    \draw (4.25,-2.5) node {$i$};
    \draw (-1,1)   rectangle (4.75,-4.25);
    \draw (4.5,-4) node {$j$};
    
    % unabhängige Variablen
    \node[manifest] (ind1) {$x_1$};
    \node[manifest] (ind2) [below=1 of ind1] {$x_2$};

    % abhängige Variable
    \node[manifest] (dep1) [above right=0 and 2 of ind2] {$y$};

    \node[latent]   (ind3) [below=1.5 of dep1] {$\zeta_0$};
    \node[latent]   (ind4) [left=1 of ind3] {$\zeta_1$};
    
    % Residuum
    \node[resid] (resdep1) [right=.5 of dep1] {$\epsilon$};
    \node[resid] (resdep1) [right=.5 of dep1] {$\epsilon$};
    \node[resid] (resdep1) [right=.5 of dep1] {$\epsilon$};

    % Zusammenhänge
    \draw [coef] (ind1) to node[auto] {$\beta_1$} (dep1); 
    \draw [coef] (ind2) to node[auto] {$\beta_2$} (dep1);
    \node[resid] (int1) [above right=0 and 1 of ind2] {};
    \draw [coef] (ind4) to (int1); 
    \draw [coef] (ind3) to (dep1); 
    \draw [coef] (resdep1) -- (dep1); 
    \draw [corr] (ind3.south west) to [bend left] (ind4.south east); 

  \end{tikzpicture} \\
  \vspace{1em}
  Formel: $y_{ij} = \beta_1 x_{1ij} + \beta_2 x_{2ij} 
    + \zeta_{0j} + \zeta_{1j} x_{2ij} + \epsilon_{ij}$
\end{frame}






%-------------------------------------------------------------------
\end{document}
