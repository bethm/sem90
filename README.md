# Latent Variable Modelling in a Nutshelll
### Oder: Strukturgleichungsmodelle in 90 Minuten

[Präsentation als PDF](tex/sem90_slides.pdf)

Mit diesem Foliensatz soll es möglich sein eine erste Hinführung zur Anwendung
von Strukturgleichungsmodellen in den Sozialwissenschaften innerhalb von 90 
Minuten zu vermitteln. Mein Hauptziel ist es dabei einen visuellen Eindruck der 
Möglichkeiten der statistischen Modellierung mit latenten Variablen zu geben und
sie anhand von konkreten Beispielen vorzuführen.

Verbesserungsvorschläge sind natürlich immer willkommen!

## Vorbereitung
Die Kursteilnehmer sollten über eine Stata-Installation (> 12.0) verfügen und sich
die Daten des ALLBUS 2012 im Stata-Format bei der [Gesis](http://www.gesis.org/allbus) 
heruntergeladen haben. Der erste Teil des [do-Files](do/sem90_examples.do) bereitet einen kleinen 
Arbeitsdatensatz auf. Der zweite Teil beinhaltet die Beispiele aus der Präsentation 
zum Mitklicken. Im Anschluss können die Beispiele dann gut als Grundlage zum eigenen 
Ausprobieren genutzt werden.

## Lizenz
![Creative Commons Lizenzvertrag](https://i.creativecommons.org/l/by/4.0/88x31.png)

"Latent Variable Modelling in a Nutshelll - Oder: Strukturgleichungsmodelle in 90 Minuten" von Arne Bethmann ist lizenziert unter einer [Creative Commons Namensnennung 4.0 International Lizenz](http://creativecommons.org/licenses/by/4.0/).