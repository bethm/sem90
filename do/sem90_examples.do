************************************************************
** Latent Variable Modeling in a Nutshell
** Examples
** Arne Bethmann
************************************************************
global path "<Enter proper path here>"

log using "$path\log\a_prep.log", replace text
********************
** A Prepare data
********************
** The data used here are from ALLBUS 2012:
** http://www.gesis.org/allbus

** Load raw data (ALLBUS 2012)
use "$path\dta\ZA4614_v1-1-1.dta", clear

** ID variable
gen id=v2
label variable id "Identifikationsnummer des Befragten"
isid id

** Gender role attitudes
clonevar item1=v102
recode item1 0 8 9 =.
tab v102 item1, m

clonevar item2=v103
recode item2 0 8 9=.		
tab v103 item2, m

clonevar item3=v104
recode item3 0 8 9=.		
tab v104 item3, m

clonevar item4=v105
recode item4 0 8 9=.		
tab v105 item4, m

clonevar item5=v106
recode item5 0 8 9=.		
tab v106 item5, m

clonevar item6=v107
recode item6 0 8 9=.		
tab v107 item6, m

** Dummy east/west Germany
rename v8 ostwest
tab ostwest, m

** Gender
rename v217 geschl
tab geschl, m

** Age
clonevar alter=v220
recode alter 999=.
tab v220 alter, m

** Marital status
clonevar familie=v274
recode familie 99 =.
tab v274 familie, m

** School education
clonevar schul=v230
recode schul 7 99 =.
tab v230 schul, m

** Vocational training
gen beruf = .
label variable beruf "Berufsbildungsabschluss"
replace beruf = 0 if inrange(v232,0,1) | inrange(v233,0,1) | inrange(v234,0,1) ///
  | inrange(v235,0,1) | inrange(v236,0,1) | inrange(v237,0,1) | inrange(v238,0,1) ///
  | inrange(v239,0,1) | inrange(v240,0,1) | inrange(v241,0,1) | inrange(v242,0,1)
replace beruf = 1 if v233 == 1 | v234 == 1 | v236 == 1 | v237 == 1 | v238 == 1
replace beruf = 2 if v239 == 1 | v240 == 1
label define beruf 0 "kein Abschluss" 1 "Ausbildung o. �." 2 "Hochschulabschluss"
label value beruf beruf
tab beruf, m

** Number of minors in HH (w/o respondent)
egen anz_mindj = anycount(v357 v367 v377 v387 v397 v407 v417), values(0/17)
label variable anz_mindj "Anzahl minderj�hriger Personen im HH(ohne befragte Person)"
tab anz_mindj, m

** Number of children under 3 in HH
egen anz_u3j = anycount(v357 v367 v377 v387 v397 v407 v417), values(0/2)
label variable anz_u3j "Anzahl Kinder unter 3 im HH"
tab anz_u3j, m

keep id item* geschl alter schul beruf familie anz_mindj anz_u3j ostwest
order id item* geschl alter schul beruf familie anz_mindj anz_u3j ostwest

save "$path\dta\work.dta", replace
********************
log close


log using "$path\log\b_ex.log", replace text
********************
** B Examples
********************

** Load dataset
use "$path\dta\work.dta", clear

** Simple Regression
regress item1 i.geschl // regression command
sem geschl -> item1 // SEM command

** Multiple Regression
regress item1 geschl alter ostwest // regression command
sem geschl alter ostwest -> item1 // SEM command

** Simple Measurement Model
*sem item1 <- GR
** Does not work, not identified

** Confirmatory Factor Analysis (CFA)
sem item1 item2 item3 <- GR 

** Latent Regression / CFA with covariates
sem (item1 item2 item3 <- GR) /// measurement model
    (geschl ostwest -> GR)    //  structural model

** Simple Path Model
sem (geschl ostwest -> item1) (geschl -> ostwest)

** (Simple) Structural Equation Model
sem (item1 item2 item3 <- GR) /// measurement model
    (geschl ostwest -> GR)    /// structural model
	(geschl -> ostwest)

********************
log close
